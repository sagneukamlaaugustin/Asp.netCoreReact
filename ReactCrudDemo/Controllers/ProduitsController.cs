﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ReactCrudDemo.Models.BML.TP;
using ReactCrudDemo.Models.DAL;
using ReactCrudDemo.Models.VML.TP;

namespace ReactCrudDemo.Controllers
{
     [Produces("application/json")]
    // [Route("api/Produits")]
    public class ProduitsController : Controller
    {
        // GET: api/Produits
        [HttpGet]
        [Route("api/Produits/Index")]
        public async Task<IEnumerable<ProduitVM>>  Index()
        {
            ProduitBM _contex = new ProduitBM();
            return await _contex.GetAllData();
        }

        // POST: api/Produits
        [HttpPost]
        [Route("api/Produits/Create")]
        public async Task<IActionResult> Create(ProduitVM Param_Var)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ProduitBM _contex = new ProduitBM();
            await _contex.SaveAsync(Param_Var);
            return CreatedAtAction("GetProduits", new { id = Param_Var.ID_Produit }, Param_Var);
        }

        // GET: api/Produits/5
        [HttpGet]
        [Route("api/Produits/Details/{id}")]
        public IActionResult Details(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ProduitBM _contex = new ProduitBM();
            var tpProduit = _contex.GetByID(id);

            if (tpProduit == null)
            {
                return NotFound();
            }

            return Ok(tpProduit);
        }

        // PUT: api/Produits/5
        [HttpPut]
        [Route("api/Produits/Edit")]
        public async Task<IActionResult> Edit(ProduitVM Param_Var)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ProduitBM _contex = new ProduitBM();
            TpProduit Local_Var = new TpProduit();
            Local_Var = await _contex.GetItemAsync(Param_Var.ID_Produit);
            if (Local_Var != null)
            {
                await _contex.EditAsync(Local_Var, Param_Var); 
            }
            else
            {
                return NotFound();
            }
            var test = CreatedAtAction("GetProduits", new { id = Param_Var.ID_Produit }, Param_Var);
            return test;
        }

        // DELETE: api/Produits/5
        [HttpDelete]
        [Route("api/Produits/Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ProduitBM _contex = new ProduitBM();
            var Local_Var = await _contex.GetItemAsync(id);
            if (Local_Var == null)
            {
                return NotFound();
            }
            await _contex.DelAsync(Local_Var);

            return Ok(Local_Var);
        }
    }
}