﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ReactCrudDemo.Models.BML.TP;
using ReactCrudDemo.Models.VML.TP;

namespace ReactCrudDemo.Controllers
{
    [Produces("application/json")]
   // [Route("api/Famille")]
    public class FamilleController : Controller
    {
        // GET: api/Famille
        [HttpGet]
        [Route("api/Famille/Index")]
        public async Task<IEnumerable<FamilleVM>> Index()
        {
            FamilleBM _contex = new FamilleBM();
            return await _contex.GetAllData();
        }
    }
}