﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ReactCrudDemo.Models.BML.TP;
using ReactCrudDemo.Models.VML.TP;

namespace ReactCrudDemo.Controllers
{
    [Produces("application/json")]
   // [Route("api/Emballage")]
    public class EmballageController : Controller
    {
        // GET: api/Produits
        [HttpGet]
        [Route("api/Emballage/Index")]
        public async Task<IEnumerable<EmballageVM>> Index()
        {
            EmballageBM _contex = new EmballageBM();
            return await _contex.GetAllData();
        }
    }
}