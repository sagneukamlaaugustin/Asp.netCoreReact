﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpRegulationStock
    {
        public long IdRegulationStock { get; set; }
        public int IdElement { get; set; }
        public byte TypeElement { get; set; }
        public string NumDoc { get; set; }
        public DateTime Date { get; set; }
        public int QuantiteCasier { get; set; }
        public int QuantitePlastique { get; set; }
        public int QuantiteBouteille { get; set; }
        public string Motif { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }
        public byte Etat { get; set; }
    }
}
