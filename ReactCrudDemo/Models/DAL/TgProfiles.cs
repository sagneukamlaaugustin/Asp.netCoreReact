﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TgProfiles
    {
        public TgProfiles()
        {
            TgDroits = new HashSet<TgDroits>();
            TgProfilesUtilisateurs = new HashSet<TgProfilesUtilisateurs>();
        }

        public int CodeProfile { get; set; }
        public string Libelle { get; set; }
        public bool Display { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }
        public bool? Actif { get; set; }

        public ICollection<TgDroits> TgDroits { get; set; }
        public ICollection<TgProfilesUtilisateurs> TgProfilesUtilisateurs { get; set; }
    }
}
