﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpInventaireDetail
    {
        public long IdInventaireDetail { get; set; }
        public long IdInventaire { get; set; }
        public int IdElement { get; set; }
        public int QuantiteCasierTheorique { get; set; }
        public int QuantitePlastiqueTheorique { get; set; }
        public int QuantiteBouteilleTheorique { get; set; }
        public int QuantiteCasierReel { get; set; }
        public int QuantitePlastiqueReel { get; set; }
        public int QuantiteBouteilleReel { get; set; }
        public double PuCasierBouteille { get; set; }
        public double PuPlastique { get; set; }
        public int Conditionnement { get; set; }

        public TpInventaire IdInventaireNavigation { get; set; }
    }
}
