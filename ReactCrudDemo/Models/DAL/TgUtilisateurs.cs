﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TgUtilisateurs
    {
        public TgUtilisateurs()
        {
            TgProfilesUtilisateurs = new HashSet<TgProfilesUtilisateurs>();
        }

        public int CodeUser { get; set; }
        public int IdPersonnel { get; set; }
        public string Login { get; set; }
        public string SommeCtrl { get; set; }
        public bool Display { get; set; }
        public DateTime LastConnexion { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }
        public bool? Actif { get; set; }

        public TgPersonnels IdPersonnelNavigation { get; set; }
        public ICollection<TgProfilesUtilisateurs> TgProfilesUtilisateurs { get; set; }
    }
}
