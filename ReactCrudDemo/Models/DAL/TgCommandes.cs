﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TgCommandes
    {
        public TgCommandes()
        {
            TgDroits = new HashSet<TgDroits>();
            TgHierarchieCommande = new HashSet<TgHierarchieCommande>();
        }

        public int CodeCommande { get; set; }
        public int CodeCommandeParent { get; set; }
        public string Libelle { get; set; }
        public string IdControl { get; set; }
        public string Description { get; set; }
        public int Ordre { get; set; }
        public string Url { get; set; }

        public ICollection<TgDroits> TgDroits { get; set; }
        public ICollection<TgHierarchieCommande> TgHierarchieCommande { get; set; }
    }
}
