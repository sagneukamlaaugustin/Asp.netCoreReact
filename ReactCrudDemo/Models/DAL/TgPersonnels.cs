﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TgPersonnels
    {
        public TgPersonnels()
        {
            TgUtilisateurs = new HashSet<TgUtilisateurs>();
            TpOuvertureClotureCaisse = new HashSet<TpOuvertureClotureCaisse>();
        }

        public int IdPersonnel { get; set; }
        public int IdCaisse { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }
        public bool? Actif { get; set; }
        public bool Display { get; set; }

        public TpCaisse IdCaisseNavigation { get; set; }
        public ICollection<TgUtilisateurs> TgUtilisateurs { get; set; }
        public ICollection<TpOuvertureClotureCaisse> TpOuvertureClotureCaisse { get; set; }
    }
}
