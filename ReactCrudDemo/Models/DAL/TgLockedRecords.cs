﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TgLockedRecords
    {
        public int IdLocked { get; set; }
        public string TableName { get; set; }
        public int RecordId { get; set; }
        public string ComputerName { get; set; }
        public DateTime LockDateTime { get; set; }
        public int CodeUser { get; set; }
    }
}
