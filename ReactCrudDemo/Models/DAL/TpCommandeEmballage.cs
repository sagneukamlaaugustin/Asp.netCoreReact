﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpCommandeEmballage
    {
        public long IdCommandeEmballage { get; set; }
        public long IdCommande { get; set; }
        public int IdEmballage { get; set; }
        public double? PuPlastique { get; set; }
        public double? PuBouteille { get; set; }
        public int? Conditionnement { get; set; }
        public int QuantiteCasier { get; set; }
        public int QuantitePlastique { get; set; }
        public int QuantiteBouteille { get; set; }

        public TpCommande IdCommandeNavigation { get; set; }
        public TpEmballage IdEmballageNavigation { get; set; }
    }
}
