﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TgVariable
    {
        public int IdVariable { get; set; }
        public string NomSociete { get; set; }
        public string NumeroContribuable { get; set; }
        public string RegistreCommerce { get; set; }
        public string Telephone { get; set; }
        public string Adresse { get; set; }
        public string Description { get; set; }
        public string Ville { get; set; }
        public byte[] Logo { get; set; }
        public bool? AppliquerTva { get; set; }
        public double? Tva { get; set; }
        public bool? GestionEmballage { get; set; }
        public bool? VerifierEtatStock { get; set; }
        public string Cle0 { get; set; }
        public byte[] Cle1 { get; set; }
        public byte[] Cle2 { get; set; }
    }
}
