﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TgCorbeille
    {
        public int IdCorbeille { get; set; }
        public int IdTable { get; set; }
        public int IdElement { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public int CodeUser { get; set; }
        public bool? Actif { get; set; }
    }
}
