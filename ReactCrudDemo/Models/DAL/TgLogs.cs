﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TgLogs
    {
        public int IdError { get; set; }
        public string ErrorValue { get; set; }
        public DateTime Date { get; set; }
        public int CodeUser { get; set; }
        public string NomPosteTravail { get; set; }
        public string Fonction { get; set; }
    }
}
