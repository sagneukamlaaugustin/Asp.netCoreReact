﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TgSentinelles
    {
        public int IdSentinelle { get; set; }
        public DateTime Date { get; set; }
        public int CodeUser { get; set; }
        public int Action { get; set; }
        public string Description { get; set; }
    }
}
