﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpRistourneClient
    {
        public int IdRistourneClient { get; set; }
        public int IdClient { get; set; }
        public int IdProduit { get; set; }
        public DateTime Date { get; set; }
        public double Ristourne { get; set; }
        public bool CumulerRistourne { get; set; }
        public bool Actif { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateCodeUser { get; set; }
        public DateTime EditDate { get; set; }
        public int EditCodeUser { get; set; }

        public TpClient IdClientNavigation { get; set; }
        public TpProduit IdProduitNavigation { get; set; }
    }
}
