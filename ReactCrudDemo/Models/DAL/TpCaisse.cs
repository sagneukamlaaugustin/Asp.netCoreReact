﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpCaisse
    {
        public TpCaisse()
        {
            TgPersonnels = new HashSet<TgPersonnels>();
            TpDecaissement = new HashSet<TpDecaissement>();
            TpEncaissement = new HashSet<TpEncaissement>();
            TpOuvertureClotureCaisse = new HashSet<TpOuvertureClotureCaisse>();
            TpVente = new HashSet<TpVente>();
        }

        public int IdCaisse { get; set; }
        public string Libelle { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }
        public bool? Actif { get; set; }

        public ICollection<TgPersonnels> TgPersonnels { get; set; }
        public ICollection<TpDecaissement> TpDecaissement { get; set; }
        public ICollection<TpEncaissement> TpEncaissement { get; set; }
        public ICollection<TpOuvertureClotureCaisse> TpOuvertureClotureCaisse { get; set; }
        public ICollection<TpVente> TpVente { get; set; }
    }
}
