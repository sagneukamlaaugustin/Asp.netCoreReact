﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TgHierarchieCommande
    {
        public int IdHierarchie { get; set; }
        public int CodeCommande { get; set; }
        public int CodeCommandeParent { get; set; }

        public TgCommandes CodeCommandeParentNavigation { get; set; }
    }
}
