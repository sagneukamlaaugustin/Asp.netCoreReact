﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpOuvertureClotureCaisse
    {
        public int IdOuvertureClotureCaisse { get; set; }
        public int IdCaisse { get; set; }
        public DateTime DateOuverture { get; set; }
        public DateTime? DateCloture { get; set; }
        public double SoldeCaisse { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }
        public byte Etat { get; set; }
        public int IdPersonnel { get; set; }

        public TpCaisse IdCaisseNavigation { get; set; }
        public TgPersonnels IdPersonnelNavigation { get; set; }
    }
}
