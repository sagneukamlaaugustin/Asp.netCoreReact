﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpVenteLiquide
    {
        public long IdVenteLiquide { get; set; }
        public long IdVente { get; set; }
        public int IdProduit { get; set; }
        public double? PuVente { get; set; }
        public double? PuAchat { get; set; }
        public int? Conditionnement { get; set; }
        public double? PuPlastique { get; set; }
        public double? PuBouteille { get; set; }
        public int QuantiteCasierLiquide { get; set; }
        public int QuantiteBouteilleLiquide { get; set; }
        public int QuantitePlastiqueEmballage { get; set; }
        public int QuantiteBouteilleEmballage { get; set; }
        public int QuantiteCasierEmballage { get; set; }
        public int? RistourneClient { get; set; }
        public bool? ProduitDecomposer { get; set; }
        public bool? CumulerRistourne { get; set; }

        public TpProduit IdProduitNavigation { get; set; }
        public TpVente IdVenteNavigation { get; set; }
    }
}
