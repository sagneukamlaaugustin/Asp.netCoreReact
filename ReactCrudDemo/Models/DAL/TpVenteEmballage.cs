﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpVenteEmballage
    {
        public long IdVenteEmballage { get; set; }
        public long IdVente { get; set; }
        public int IdEmballage { get; set; }
        public int QuantiteCasier { get; set; }
        public int QuantitePlastique { get; set; }
        public int QuantiteBouteille { get; set; }
        public int Conditionnement { get; set; }
        public double PuPlastique { get; set; }
        public double PuBouteille { get; set; }

        public TpEmballage IdEmballageNavigation { get; set; }
        public TpVente IdVenteNavigation { get; set; }
    }
}
