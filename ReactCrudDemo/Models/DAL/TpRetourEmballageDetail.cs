﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpRetourEmballageDetail
    {
        public long IdRetourEmballageDetail { get; set; }
        public long IdRetourEmballage { get; set; }
        public int IdEmballage { get; set; }
        public double PuPlastique { get; set; }
        public double PuBouteille { get; set; }
        public int Conditionnement { get; set; }
        public int QuantiteCasier { get; set; }
        public int QuantitePlastique { get; set; }
        public int QuantiteBouteille { get; set; }

        public TpEmballage IdEmballageNavigation { get; set; }
        public TpRetourEmballage IdRetourEmballageNavigation { get; set; }
    }
}
