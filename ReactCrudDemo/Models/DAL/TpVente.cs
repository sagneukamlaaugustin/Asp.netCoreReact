﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpVente
    {
        public TpVente()
        {
            TpVenteEmballage = new HashSet<TpVenteEmballage>();
            TpVenteLiquide = new HashSet<TpVenteLiquide>();
        }

        public long IdVente { get; set; }
        public int IdClient { get; set; }
        public int IdCaisse { get; set; }
        public DateTime Date { get; set; }
        public string NumDoc { get; set; }
        public string NomClient { get; set; }
        public double MontantConsigneEmballage { get; set; }
        public double Montant { get; set; }
        public double? SoldeLiquide { get; set; }
        public double? SoldeEmballage { get; set; }
        public byte Etat { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }

        public TpCaisse IdCaisseNavigation { get; set; }
        public TpClient IdClientNavigation { get; set; }
        public ICollection<TpVenteEmballage> TpVenteEmballage { get; set; }
        public ICollection<TpVenteLiquide> TpVenteLiquide { get; set; }
    }
}
