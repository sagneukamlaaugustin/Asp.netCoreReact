﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TgProfilesUtilisateurs
    {
        public int IdProfilesUtilisateur { get; set; }
        public int CodeUser { get; set; }
        public int CodeProfile { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }
        public bool? Actif { get; set; }

        public TgProfiles CodeProfileNavigation { get; set; }
        public TgUtilisateurs CodeUserNavigation { get; set; }
    }
}
