﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpEncaissement
    {
        public long IdEncaissement { get; set; }
        public int IdClient { get; set; }
        public int IdCaissse { get; set; }
        public string NumDoc { get; set; }
        public DateTime Date { get; set; }
        public double Montant { get; set; }
        public string Motif { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }
        public byte Etat { get; set; }

        public TpCaisse IdCaissseNavigation { get; set; }
        public TpClient IdClientNavigation { get; set; }
    }
}
