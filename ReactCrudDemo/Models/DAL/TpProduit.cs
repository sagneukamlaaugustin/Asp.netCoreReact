﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpProduit
    {
        public TpProduit()
        {
            InverseIdProduitBaseNavigation = new HashSet<TpProduit>();
            TpCommandeLiquide = new HashSet<TpCommandeLiquide>();
            TpCommandeLiquideTemp = new HashSet<TpCommandeLiquideTemp>();
            TpRistourneClient = new HashSet<TpRistourneClient>();
            TpVenteLiquide = new HashSet<TpVenteLiquide>();
            TpVenteLiquideTemp = new HashSet<TpVenteLiquideTemp>();
        }

        public int IdProduit { get; set; }
        public int? IdProduitBase { get; set; }
        public int IdFamille { get; set; }
        public int IdEmballage { get; set; }
        public string Code { get; set; }
        public string Libelle { get; set; }
        public double PuAchat { get; set; }
        public double PuVente { get; set; }
        public bool ValeurEmballage { get; set; }
        public bool ProduitDecomposer { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }
        public bool? Actif { get; set; }
        public int? IdPlastiqueBase { get; set; }
        public int? IdBouteilleBase { get; set; }

        public TpEmballage IdBouteilleBaseNavigation { get; set; }
        public TpEmballage IdEmballageNavigation { get; set; }
        public TpFamille IdFamilleNavigation { get; set; }
        public TpEmballage IdPlastiqueBaseNavigation { get; set; }
        public TpProduit IdProduitBaseNavigation { get; set; }
        public ICollection<TpProduit> InverseIdProduitBaseNavigation { get; set; }
        public ICollection<TpCommandeLiquide> TpCommandeLiquide { get; set; }
        public ICollection<TpCommandeLiquideTemp> TpCommandeLiquideTemp { get; set; }
        public ICollection<TpRistourneClient> TpRistourneClient { get; set; }
        public ICollection<TpVenteLiquide> TpVenteLiquide { get; set; }
        public ICollection<TpVenteLiquideTemp> TpVenteLiquideTemp { get; set; }
    }
}
