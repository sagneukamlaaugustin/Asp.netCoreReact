﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpFamille
    {
        public TpFamille()
        {
            TpProduit = new HashSet<TpProduit>();
        }

        public int IdFamille { get; set; }
        public string Libelle { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }
        public bool? Actif { get; set; }

        public ICollection<TpProduit> TpProduit { get; set; }
    }
}
