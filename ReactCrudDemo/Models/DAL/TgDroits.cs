﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TgDroits
    {
        public int IdDroit { get; set; }
        public int CodeProfile { get; set; }
        public int CodeCommande { get; set; }
        public bool Executer { get; set; }
        public bool? Disponible { get; set; }

        public TgCommandes CodeCommandeNavigation { get; set; }
        public TgProfiles CodeProfileNavigation { get; set; }
    }
}
