﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpClient
    {
        public TpClient()
        {
            TpEncaissement = new HashSet<TpEncaissement>();
            TpRistourneClient = new HashSet<TpRistourneClient>();
            TpVente = new HashSet<TpVente>();
            TpVenteLiquideTemp = new HashSet<TpVenteLiquideTemp>();
        }

        public int IdClient { get; set; }
        public string Nom { get; set; }
        public string Adresse { get; set; }
        public string Telephone { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }
        public bool? Actif { get; set; }

        public ICollection<TpEncaissement> TpEncaissement { get; set; }
        public ICollection<TpRistourneClient> TpRistourneClient { get; set; }
        public ICollection<TpVente> TpVente { get; set; }
        public ICollection<TpVenteLiquideTemp> TpVenteLiquideTemp { get; set; }
    }
}
