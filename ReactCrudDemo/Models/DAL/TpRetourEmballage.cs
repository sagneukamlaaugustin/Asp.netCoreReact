﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpRetourEmballage
    {
        public TpRetourEmballage()
        {
            TpRetourEmballageDetail = new HashSet<TpRetourEmballageDetail>();
        }

        public long IdRetourEmballage { get; set; }
        public byte TypeElement { get; set; }
        public int IdElement { get; set; }
        public string NumDoc { get; set; }
        public DateTime Date { get; set; }
        public byte Etat { get; set; }
        public string Motif { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }

        public ICollection<TpRetourEmballageDetail> TpRetourEmballageDetail { get; set; }
    }
}
