﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpCommandeLiquideTemp
    {
        public long IdCommandeLiquideTemp { get; set; }
        public int IdProduit { get; set; }
        public double? PuAchat { get; set; }
        public int? Conditionnement { get; set; }
        public double? PuPlastique { get; set; }
        public double? PuBouteille { get; set; }
        public int QuantiteCasierLiquide { get; set; }
        public int QuantiteBouteilleLiquide { get; set; }
        public int QuantiteCasierEmballage { get; set; }
        public int QuantitePlastiqueEmballage { get; set; }
        public int QuantiteBouteilleEmballage { get; set; }
        public int CreateCodeUser { get; set; }

        public TpProduit IdProduitNavigation { get; set; }
    }
}
