﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpDecaissement
    {
        public long IdDecaissement { get; set; }
        public int IdCaisse { get; set; }
        public byte TypeElement { get; set; }
        public int IdElement { get; set; }
        public string NumDoc { get; set; }
        public DateTime Date { get; set; }
        public double Montant { get; set; }
        public string Motif { get; set; }
        public byte Etat { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }

        public TpCaisse IdCaisseNavigation { get; set; }
    }
}
