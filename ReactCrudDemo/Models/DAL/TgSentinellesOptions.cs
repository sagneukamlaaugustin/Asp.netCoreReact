﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TgSentinellesOptions
    {
        public int IdSentinelle { get; set; }
        public bool Sentinelle { get; set; }
        public bool SentinelleConnexion { get; set; }
        public bool SentinelleEndConnexion { get; set; }
        public bool SentinelleInsert { get; set; }
        public bool SentinelleEdit { get; set; }
        public bool SentinelleDelete { get; set; }
        public bool SentinellePrint { get; set; }
        public int SentinelleLife { get; set; }
    }
}
