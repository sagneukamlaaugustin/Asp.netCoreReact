﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpInventaire
    {
        public TpInventaire()
        {
            TpInventaireDetail = new HashSet<TpInventaireDetail>();
        }

        public long IdInventaire { get; set; }
        public byte Type { get; set; }
        public string NumDoc { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime? DateFin { get; set; }
        public byte Etat { get; set; }
        public string Motif { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }

        public ICollection<TpInventaireDetail> TpInventaireDetail { get; set; }
    }
}
