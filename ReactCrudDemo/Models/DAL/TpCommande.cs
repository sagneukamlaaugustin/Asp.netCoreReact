﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpCommande
    {
        public TpCommande()
        {
            TpCommandeEmballage = new HashSet<TpCommandeEmballage>();
            TpCommandeLiquide = new HashSet<TpCommandeLiquide>();
        }

        public long IdCommande { get; set; }
        public int IdFournisseur { get; set; }
        public DateTime Date { get; set; }
        public double? Montant { get; set; }
        public string NumDoc { get; set; }
        public byte Etat { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }

        public TpFournisseur IdFournisseurNavigation { get; set; }
        public ICollection<TpCommandeEmballage> TpCommandeEmballage { get; set; }
        public ICollection<TpCommandeLiquide> TpCommandeLiquide { get; set; }
    }
}
