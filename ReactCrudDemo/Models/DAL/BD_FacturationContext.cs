﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ReactCrudDemo.Models.DAL
{
    public partial class BD_FacturationContext : DbContext
    {
        public BD_FacturationContext()
        {
        }

        public BD_FacturationContext(DbContextOptions<BD_FacturationContext> options)
            : base(options)
        {
        }
        public virtual DbSet<TgCommandes> TgCommandes { get; set; }
        public virtual DbSet<TgCorbeille> TgCorbeille { get; set; }
        public virtual DbSet<TgDroits> TgDroits { get; set; }
        public virtual DbSet<TgHierarchieCommande> TgHierarchieCommande { get; set; }
        public virtual DbSet<TgLockedRecords> TgLockedRecords { get; set; }
        public virtual DbSet<TgLogs> TgLogs { get; set; }
        public virtual DbSet<TgPersonnels> TgPersonnels { get; set; }
        public virtual DbSet<TgProfiles> TgProfiles { get; set; }
        public virtual DbSet<TgProfilesUtilisateurs> TgProfilesUtilisateurs { get; set; }
        public virtual DbSet<TgSentinelles> TgSentinelles { get; set; }
        public virtual DbSet<TgSentinellesOptions> TgSentinellesOptions { get; set; }
        public virtual DbSet<TgUtilisateurs> TgUtilisateurs { get; set; }
        public virtual DbSet<TgVariable> TgVariable { get; set; }
        public virtual DbSet<TpCaisse> TpCaisse { get; set; }
        public virtual DbSet<TpClient> TpClient { get; set; }
        public virtual DbSet<TpCommande> TpCommande { get; set; }
        public virtual DbSet<TpCommandeEmballage> TpCommandeEmballage { get; set; }
        public virtual DbSet<TpCommandeEmballageTemp> TpCommandeEmballageTemp { get; set; }
        public virtual DbSet<TpCommandeLiquide> TpCommandeLiquide { get; set; }
        public virtual DbSet<TpCommandeLiquideTemp> TpCommandeLiquideTemp { get; set; }
        public virtual DbSet<TpDecaissement> TpDecaissement { get; set; }
        public virtual DbSet<TpEmballage> TpEmballage { get; set; }
        public virtual DbSet<TpEncaissement> TpEncaissement { get; set; }
        public virtual DbSet<TpFamille> TpFamille { get; set; }
        public virtual DbSet<TpFournisseur> TpFournisseur { get; set; }
        public virtual DbSet<TpInventaire> TpInventaire { get; set; }
        public virtual DbSet<TpInventaireDetail> TpInventaireDetail { get; set; }
        public virtual DbSet<TpOuvertureClotureCaisse> TpOuvertureClotureCaisse { get; set; }
        public virtual DbSet<TpProduit> TpProduit { get; set; }
        public virtual DbSet<TpRegulationSolde> TpRegulationSolde { get; set; }
        public virtual DbSet<TpRegulationStock> TpRegulationStock { get; set; }
        public virtual DbSet<TpRetourEmballage> TpRetourEmballage { get; set; }
        public virtual DbSet<TpRetourEmballageDetail> TpRetourEmballageDetail { get; set; }
        public virtual DbSet<TpRetourEmballageTemp> TpRetourEmballageTemp { get; set; }
        public virtual DbSet<TpRistourneClient> TpRistourneClient { get; set; }
        public virtual DbSet<TpTypeDepense> TpTypeDepense { get; set; }
        public virtual DbSet<TpVente> TpVente { get; set; }
        public virtual DbSet<TpVenteEmballage> TpVenteEmballage { get; set; }
        public virtual DbSet<TpVenteEmballageTemp> TpVenteEmballageTemp { get; set; }
        public virtual DbSet<TpVenteLiquide> TpVenteLiquide { get; set; }
        public virtual DbSet<TpVenteLiquideTemp> TpVenteLiquideTemp { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Data Source=QUBE-GROUP-POST;Initial Catalog=BD_Facturation;User ID=admin;Password=admin");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TgCommandes>(entity =>
            {
                entity.HasKey(e => e.CodeCommande);

                entity.ToTable("TG_Commandes");

                entity.Property(e => e.CodeCommande).HasColumnName("Code_Commande");

                entity.Property(e => e.CodeCommandeParent).HasColumnName("Code_Commande_Parent");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.IdControl)
                    .IsRequired()
                    .HasColumnName("ID_Control")
                    .HasMaxLength(50);

                entity.Property(e => e.Libelle)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TgCorbeille>(entity =>
            {
                entity.HasKey(e => e.IdCorbeille);

                entity.ToTable("TG_Corbeille");

                entity.Property(e => e.IdCorbeille).HasColumnName("ID_Corbeille");

                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.Property(e => e.CodeUser).HasColumnName("Code_User");

                entity.Property(e => e.Date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IdElement).HasColumnName("ID_Element");

                entity.Property(e => e.IdTable).HasColumnName("ID_Table");
            });

            modelBuilder.Entity<TgDroits>(entity =>
            {
                entity.HasKey(e => e.IdDroit);

                entity.ToTable("TG_Droits");

                entity.Property(e => e.IdDroit).HasColumnName("ID_Droit");

                entity.Property(e => e.CodeCommande).HasColumnName("Code_Commande");

                entity.Property(e => e.CodeProfile).HasColumnName("Code_Profile");

                entity.Property(e => e.Disponible).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CodeCommandeNavigation)
                    .WithMany(p => p.TgDroits)
                    .HasForeignKey(d => d.CodeCommande)
                    .HasConstraintName("TG_Commandes_TG_Droits_FK1");

                entity.HasOne(d => d.CodeProfileNavigation)
                    .WithMany(p => p.TgDroits)
                    .HasForeignKey(d => d.CodeProfile)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TG_Profiles_TG_Droits_FK1");
            });

            modelBuilder.Entity<TgHierarchieCommande>(entity =>
            {
                entity.HasKey(e => e.IdHierarchie);

                entity.ToTable("TG_Hierarchie_Commande");

                entity.Property(e => e.IdHierarchie).HasColumnName("ID_Hierarchie");

                entity.Property(e => e.CodeCommande).HasColumnName("Code_Commande");

                entity.Property(e => e.CodeCommandeParent).HasColumnName("Code_Commande_Parent");

                entity.HasOne(d => d.CodeCommandeParentNavigation)
                    .WithMany(p => p.TgHierarchieCommande)
                    .HasForeignKey(d => d.CodeCommandeParent)
                    .HasConstraintName("FK_TG_Hierarchie_Commande_TG_Commandes1");
            });

            modelBuilder.Entity<TgLockedRecords>(entity =>
            {
                entity.HasKey(e => e.IdLocked);

                entity.ToTable("TG_Locked_Records");

                entity.Property(e => e.IdLocked).HasColumnName("ID_Locked");

                entity.Property(e => e.CodeUser).HasColumnName("Code_User");

                entity.Property(e => e.ComputerName)
                    .IsRequired()
                    .HasColumnName("Computer_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.LockDateTime)
                    .HasColumnName("Lock_Date_Time")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RecordId).HasColumnName("Record_ID");

                entity.Property(e => e.TableName)
                    .IsRequired()
                    .HasColumnName("Table_Name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TgLogs>(entity =>
            {
                entity.HasKey(e => e.IdError);

                entity.ToTable("TG_Logs");

                entity.Property(e => e.IdError).HasColumnName("Id_Error");

                entity.Property(e => e.CodeUser).HasColumnName("Code_user");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.ErrorValue)
                    .IsRequired()
                    .HasColumnName("Error_value");

                entity.Property(e => e.Fonction)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.NomPosteTravail)
                    .IsRequired()
                    .HasColumnName("Nom_Poste_Travail")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TgPersonnels>(entity =>
            {
                entity.HasKey(e => e.IdPersonnel);

                entity.ToTable("TG_Personnels");

                entity.Property(e => e.IdPersonnel).HasColumnName("ID_Personnel");

                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdCaisse).HasColumnName("ID_Caisse");

                entity.Property(e => e.Nom)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.Prenom).HasMaxLength(150);

                entity.HasOne(d => d.IdCaisseNavigation)
                    .WithMany(p => p.TgPersonnels)
                    .HasForeignKey(d => d.IdCaisse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TG_Personnels_TP_Caisse");
            });

            modelBuilder.Entity<TgProfiles>(entity =>
            {
                entity.HasKey(e => e.CodeProfile);

                entity.ToTable("TG_Profiles");

                entity.Property(e => e.CodeProfile).HasColumnName("Code_Profile");

                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Libelle)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TgProfilesUtilisateurs>(entity =>
            {
                entity.HasKey(e => e.IdProfilesUtilisateur);

                entity.ToTable("TG_Profiles_Utilisateurs");

                entity.Property(e => e.IdProfilesUtilisateur).HasColumnName("ID_Profiles_Utilisateur");

                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.Property(e => e.CodeProfile).HasColumnName("Code_Profile");

                entity.Property(e => e.CodeUser).HasColumnName("Code_User");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.CodeProfileNavigation)
                    .WithMany(p => p.TgProfilesUtilisateurs)
                    .HasForeignKey(d => d.CodeProfile)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TG_Profiles_TG_Profiles_Utilisateurs_FK1");

                entity.HasOne(d => d.CodeUserNavigation)
                    .WithMany(p => p.TgProfilesUtilisateurs)
                    .HasForeignKey(d => d.CodeUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TG_Utilisateurs_TG_Profiles_Utilisateurs_FK1");
            });

            modelBuilder.Entity<TgSentinelles>(entity =>
            {
                entity.HasKey(e => e.IdSentinelle);

                entity.ToTable("TG_Sentinelles");

                entity.Property(e => e.IdSentinelle).HasColumnName("ID_Sentinelle");

                entity.Property(e => e.CodeUser).HasColumnName("Code_User");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<TgSentinellesOptions>(entity =>
            {
                entity.HasKey(e => e.IdSentinelle);

                entity.ToTable("TG_Sentinelles_Options");

                entity.Property(e => e.IdSentinelle).HasColumnName("ID_Sentinelle");
            });

            modelBuilder.Entity<TgUtilisateurs>(entity =>
            {
                entity.HasKey(e => e.CodeUser);

                entity.ToTable("TG_Utilisateurs");

                entity.Property(e => e.CodeUser).HasColumnName("Code_User");

                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdPersonnel).HasColumnName("ID_Personnel");

                entity.Property(e => e.LastConnexion)
                    .HasColumnName("Last_Connexion")
                    .HasColumnType("datetime");

                entity.Property(e => e.Login)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SommeCtrl)
                    .IsRequired()
                    .HasColumnName("Somme_Ctrl")
                    .HasMaxLength(50);

                entity.HasOne(d => d.IdPersonnelNavigation)
                    .WithMany(p => p.TgUtilisateurs)
                    .HasForeignKey(d => d.IdPersonnel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TG_Personnels_TG_Utilisateurs_FK1");
            });

            modelBuilder.Entity<TgVariable>(entity =>
            {
                entity.HasKey(e => e.IdVariable);

                entity.ToTable("TG_Variable");

                entity.Property(e => e.IdVariable).HasColumnName("ID_Variable");

                entity.Property(e => e.Adresse).HasMaxLength(50);

                entity.Property(e => e.AppliquerTva).HasColumnName("Appliquer_TVA");

                entity.Property(e => e.Cle0).HasMaxLength(100);

                entity.Property(e => e.Cle1).HasMaxLength(100);

                entity.Property(e => e.Cle2).HasMaxLength(100);

                entity.Property(e => e.Description).HasMaxLength(150);

                entity.Property(e => e.GestionEmballage).HasColumnName("Gestion_Emballage");

                entity.Property(e => e.NomSociete)
                    .IsRequired()
                    .HasColumnName("Nom_Societe")
                    .HasMaxLength(150);

                entity.Property(e => e.NumeroContribuable)
                    .HasColumnName("Numero_Contribuable")
                    .HasMaxLength(50);

                entity.Property(e => e.RegistreCommerce)
                    .HasColumnName("Registre_Commerce")
                    .HasMaxLength(50);

                entity.Property(e => e.Telephone).HasMaxLength(50);

                entity.Property(e => e.Tva).HasColumnName("TVA");

                entity.Property(e => e.VerifierEtatStock).HasColumnName("Verifier_Etat_Stock");

                entity.Property(e => e.Ville).HasMaxLength(50);
            });

            modelBuilder.Entity<TpCaisse>(entity =>
            {
                entity.HasKey(e => e.IdCaisse);

                entity.ToTable("TP_Caisse");

                entity.Property(e => e.IdCaisse).HasColumnName("ID_Caisse");

                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Libelle)
                    .IsRequired()
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<TpClient>(entity =>
            {
                entity.HasKey(e => e.IdClient);

                entity.ToTable("TP_Client");

                entity.Property(e => e.IdClient).HasColumnName("ID_Client");

                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.Property(e => e.Adresse).HasMaxLength(50);

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nom)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.Telephone).HasMaxLength(50);
            });

            modelBuilder.Entity<TpCommande>(entity =>
            {
                entity.HasKey(e => e.IdCommande);

                entity.ToTable("TP_Commande");

                entity.Property(e => e.IdCommande).HasColumnName("ID_Commande");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdFournisseur).HasColumnName("ID_Fournisseur");

                entity.Property(e => e.NumDoc)
                    .HasColumnName("NUM_DOC")
                    .HasMaxLength(50);

                entity.HasOne(d => d.IdFournisseurNavigation)
                    .WithMany(p => p.TpCommande)
                    .HasForeignKey(d => d.IdFournisseur)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Commande_TP_Fournisseur");
            });

            modelBuilder.Entity<TpCommandeEmballage>(entity =>
            {
                entity.HasKey(e => e.IdCommandeEmballage);

                entity.ToTable("TP_Commande_Emballage");

                entity.Property(e => e.IdCommandeEmballage).HasColumnName("ID_Commande_Emballage");

                entity.Property(e => e.IdCommande).HasColumnName("ID_Commande");

                entity.Property(e => e.IdEmballage).HasColumnName("ID_Emballage");

                entity.Property(e => e.PuBouteille).HasColumnName("PU_Bouteille");

                entity.Property(e => e.PuPlastique).HasColumnName("PU_Plastique");

                entity.Property(e => e.QuantiteBouteille).HasColumnName("Quantite_Bouteille");

                entity.Property(e => e.QuantiteCasier).HasColumnName("Quantite_Casier");

                entity.Property(e => e.QuantitePlastique).HasColumnName("Quantite_Plastique");

                entity.HasOne(d => d.IdCommandeNavigation)
                    .WithMany(p => p.TpCommandeEmballage)
                    .HasForeignKey(d => d.IdCommande)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Commande_Emballage_TP_Commande");

                entity.HasOne(d => d.IdEmballageNavigation)
                    .WithMany(p => p.TpCommandeEmballage)
                    .HasForeignKey(d => d.IdEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Commande_Emballage_TP_Emballage");
            });

            modelBuilder.Entity<TpCommandeEmballageTemp>(entity =>
            {
                entity.HasKey(e => e.IdCommandeEmballageTemp);

                entity.ToTable("TP_Commande_Emballage_Temp");

                entity.Property(e => e.IdCommandeEmballageTemp).HasColumnName("ID_Commande_Emballage_Temp");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.IdEmballage).HasColumnName("ID_Emballage");

                entity.Property(e => e.PuBouteille).HasColumnName("PU_Bouteille");

                entity.Property(e => e.PuPlastique).HasColumnName("PU_Plastique");

                entity.Property(e => e.QuantiteBouteille).HasColumnName("Quantite_Bouteille");

                entity.Property(e => e.QuantiteCasier).HasColumnName("Quantite_Casier");

                entity.Property(e => e.QuantitePlastique).HasColumnName("Quantite_Plastique");

                entity.HasOne(d => d.IdEmballageNavigation)
                    .WithMany(p => p.TpCommandeEmballageTemp)
                    .HasForeignKey(d => d.IdEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Commande_Emballage_Temp_TP_Emballage");
            });

            modelBuilder.Entity<TpCommandeLiquide>(entity =>
            {
                entity.HasKey(e => e.IdCommandeLiquide);

                entity.ToTable("TP_Commande_Liquide");

                entity.Property(e => e.IdCommandeLiquide).HasColumnName("ID_Commande_Liquide");

                entity.Property(e => e.IdCommande).HasColumnName("ID_Commande");

                entity.Property(e => e.IdProduit).HasColumnName("ID_Produit");

                entity.Property(e => e.PuAchat).HasColumnName("PU_Achat");

                entity.Property(e => e.PuBouteille).HasColumnName("PU_Bouteille");

                entity.Property(e => e.PuPlastique).HasColumnName("PU_Plastique");

                entity.Property(e => e.QuantiteBouteilleEmballage).HasColumnName("Quantite_Bouteille_Emballage");

                entity.Property(e => e.QuantiteBouteilleLiquide).HasColumnName("Quantite_Bouteille_Liquide");

                entity.Property(e => e.QuantiteCasierEmballage).HasColumnName("Quantite_Casier_Emballage");

                entity.Property(e => e.QuantiteCasierLiquide).HasColumnName("Quantite_Casier_Liquide");

                entity.Property(e => e.QuantitePlastiqueEmballage).HasColumnName("Quantite_Plastique_Emballage");

                entity.HasOne(d => d.IdCommandeNavigation)
                    .WithMany(p => p.TpCommandeLiquide)
                    .HasForeignKey(d => d.IdCommande)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Commande_Liquide_TP_Commande");

                entity.HasOne(d => d.IdProduitNavigation)
                    .WithMany(p => p.TpCommandeLiquide)
                    .HasForeignKey(d => d.IdProduit)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Commande_Liquide_TP_Produit");
            });

            modelBuilder.Entity<TpCommandeLiquideTemp>(entity =>
            {
                entity.HasKey(e => e.IdCommandeLiquideTemp);

                entity.ToTable("TP_Commande_Liquide_Temp");

                entity.Property(e => e.IdCommandeLiquideTemp).HasColumnName("ID_Commande_Liquide_Temp");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.IdProduit).HasColumnName("ID_Produit");

                entity.Property(e => e.PuAchat).HasColumnName("PU_Achat");

                entity.Property(e => e.PuBouteille).HasColumnName("PU_Bouteille");

                entity.Property(e => e.PuPlastique).HasColumnName("PU_Plastique");

                entity.Property(e => e.QuantiteBouteilleEmballage).HasColumnName("Quantite_Bouteille_Emballage");

                entity.Property(e => e.QuantiteBouteilleLiquide).HasColumnName("Quantite_Bouteille_Liquide");

                entity.Property(e => e.QuantiteCasierEmballage).HasColumnName("Quantite_Casier_Emballage");

                entity.Property(e => e.QuantiteCasierLiquide).HasColumnName("Quantite_Casier_Liquide");

                entity.Property(e => e.QuantitePlastiqueEmballage).HasColumnName("Quantite_Plastique_Emballage");

                entity.HasOne(d => d.IdProduitNavigation)
                    .WithMany(p => p.TpCommandeLiquideTemp)
                    .HasForeignKey(d => d.IdProduit)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Commande_Liquide_Temp_TP_Produit");
            });

            modelBuilder.Entity<TpDecaissement>(entity =>
            {
                entity.HasKey(e => e.IdDecaissement);

                entity.ToTable("TP_Decaissement");

                entity.Property(e => e.IdDecaissement).HasColumnName("ID_Decaissement");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Etat).HasDefaultValueSql("((1))");

                entity.Property(e => e.IdCaisse).HasColumnName("ID_Caisse");

                entity.Property(e => e.IdElement).HasColumnName("ID_Element");

                entity.Property(e => e.Motif).HasMaxLength(500);

                entity.Property(e => e.NumDoc)
                    .HasColumnName("NUM_DOC")
                    .HasMaxLength(50);

                entity.Property(e => e.TypeElement).HasColumnName("Type_Element");

                entity.HasOne(d => d.IdCaisseNavigation)
                    .WithMany(p => p.TpDecaissement)
                    .HasForeignKey(d => d.IdCaisse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Decaissement_TP_Caisse");
            });

            modelBuilder.Entity<TpEmballage>(entity =>
            {
                entity.HasKey(e => e.IdEmballage);

                entity.ToTable("TP_Emballage");

                entity.Property(e => e.IdEmballage).HasColumnName("ID_Emballage");

                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PuBouteille).HasColumnName("PU_Bouteille");

                entity.Property(e => e.PuPlastique).HasColumnName("PU_Plastique");
            });

            modelBuilder.Entity<TpEncaissement>(entity =>
            {
                entity.HasKey(e => e.IdEncaissement);

                entity.ToTable("TP_Encaissement");

                entity.Property(e => e.IdEncaissement).HasColumnName("ID_Encaissement");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Etat).HasDefaultValueSql("((1))");

                entity.Property(e => e.IdCaissse).HasColumnName("ID_Caissse");

                entity.Property(e => e.IdClient).HasColumnName("ID_Client");

                entity.Property(e => e.Motif).HasMaxLength(500);

                entity.Property(e => e.NumDoc)
                    .HasColumnName("NUM_DOC")
                    .HasMaxLength(50);

                entity.HasOne(d => d.IdCaissseNavigation)
                    .WithMany(p => p.TpEncaissement)
                    .HasForeignKey(d => d.IdCaissse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Encaissement_TP_Caisse");

                entity.HasOne(d => d.IdClientNavigation)
                    .WithMany(p => p.TpEncaissement)
                    .HasForeignKey(d => d.IdClient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Encaissement_TP_Client");
            });

            modelBuilder.Entity<TpFamille>(entity =>
            {
                entity.HasKey(e => e.IdFamille);

                entity.ToTable("TP_Famille");

                entity.Property(e => e.IdFamille).HasColumnName("ID_Famille");

                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Libelle)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TpFournisseur>(entity =>
            {
                entity.HasKey(e => e.IdFournisseur);

                entity.ToTable("TP_Fournisseur");

                entity.Property(e => e.IdFournisseur).HasColumnName("ID_Fournisseur");

                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.Property(e => e.Adresse).HasMaxLength(50);

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nom)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.Telephone).HasMaxLength(50);
            });

            modelBuilder.Entity<TpInventaire>(entity =>
            {
                entity.HasKey(e => e.IdInventaire);

                entity.ToTable("TP_Inventaire");

                entity.Property(e => e.IdInventaire).HasColumnName("ID_Inventaire");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateDebut)
                    .HasColumnName("Date_Debut")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateFin)
                    .HasColumnName("Date_Fin")
                    .HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Motif).HasMaxLength(500);

                entity.Property(e => e.NumDoc)
                    .HasColumnName("NUM_DOC")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TpInventaireDetail>(entity =>
            {
                entity.HasKey(e => e.IdInventaireDetail);

                entity.ToTable("TP_Inventaire_Detail");

                entity.Property(e => e.IdInventaireDetail).HasColumnName("ID_Inventaire_Detail");

                entity.Property(e => e.IdElement).HasColumnName("ID_Element");

                entity.Property(e => e.IdInventaire).HasColumnName("ID_Inventaire");

                entity.Property(e => e.PuCasierBouteille).HasColumnName("PU_Casier_Bouteille");

                entity.Property(e => e.PuPlastique).HasColumnName("PU_Plastique");

                entity.Property(e => e.QuantiteBouteilleReel).HasColumnName("Quantite_Bouteille_Reel");

                entity.Property(e => e.QuantiteBouteilleTheorique).HasColumnName("Quantite_Bouteille_Theorique");

                entity.Property(e => e.QuantiteCasierReel).HasColumnName("Quantite_Casier_Reel");

                entity.Property(e => e.QuantiteCasierTheorique).HasColumnName("Quantite_Casier_Theorique");

                entity.Property(e => e.QuantitePlastiqueReel).HasColumnName("Quantite_Plastique_Reel");

                entity.Property(e => e.QuantitePlastiqueTheorique).HasColumnName("Quantite_Plastique_Theorique");

                entity.HasOne(d => d.IdInventaireNavigation)
                    .WithMany(p => p.TpInventaireDetail)
                    .HasForeignKey(d => d.IdInventaire)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Inventaire_Detail_TP_Inventaire");
            });

            modelBuilder.Entity<TpOuvertureClotureCaisse>(entity =>
            {
                entity.HasKey(e => e.IdOuvertureClotureCaisse);

                entity.ToTable("TP_Ouverture_Cloture_Caisse");

                entity.Property(e => e.IdOuvertureClotureCaisse).HasColumnName("ID_Ouverture_Cloture_Caisse");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateCloture)
                    .HasColumnName("Date_Cloture")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOuverture)
                    .HasColumnName("Date_Ouverture")
                    .HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Etat).HasDefaultValueSql("((1))");

                entity.Property(e => e.IdCaisse).HasColumnName("ID_Caisse");

                entity.Property(e => e.IdPersonnel).HasColumnName("ID_Personnel");

                entity.Property(e => e.SoldeCaisse).HasColumnName("Solde_Caisse");

                entity.HasOne(d => d.IdCaisseNavigation)
                    .WithMany(p => p.TpOuvertureClotureCaisse)
                    .HasForeignKey(d => d.IdCaisse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Ouverture_Cloture_Caisse_TP_Caisse");

                entity.HasOne(d => d.IdPersonnelNavigation)
                    .WithMany(p => p.TpOuvertureClotureCaisse)
                    .HasForeignKey(d => d.IdPersonnel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Ouverture_Cloture_Caisse_TG_Personnels");
            });

            modelBuilder.Entity<TpProduit>(entity =>
            {
                entity.HasKey(e => e.IdProduit);

                entity.ToTable("TP_Produit");

                entity.Property(e => e.IdProduit).HasColumnName("ID_Produit");

                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.Property(e => e.Code).HasMaxLength(50);

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdBouteilleBase).HasColumnName("ID_Bouteille_Base");

                entity.Property(e => e.IdEmballage).HasColumnName("ID_Emballage");

                entity.Property(e => e.IdFamille).HasColumnName("ID_Famille");

                entity.Property(e => e.IdPlastiqueBase).HasColumnName("ID_Plastique_Base");

                entity.Property(e => e.IdProduitBase).HasColumnName("ID_Produit_Base");

                entity.Property(e => e.Libelle)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.ProduitDecomposer).HasColumnName("Produit_Decomposer");

                entity.Property(e => e.PuAchat).HasColumnName("PU_Achat");

                entity.Property(e => e.PuVente).HasColumnName("PU_Vente");

                entity.Property(e => e.ValeurEmballage).HasColumnName("Valeur_Emballage");

                entity.HasOne(d => d.IdBouteilleBaseNavigation)
                    .WithMany(p => p.TpProduitIdBouteilleBaseNavigation)
                    .HasForeignKey(d => d.IdBouteilleBase)
                    .HasConstraintName("FK_TP_Produit_TP_Emballage2");

                entity.HasOne(d => d.IdEmballageNavigation)
                    .WithMany(p => p.TpProduitIdEmballageNavigation)
                    .HasForeignKey(d => d.IdEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Produit_TP_Emballage");

                entity.HasOne(d => d.IdFamilleNavigation)
                    .WithMany(p => p.TpProduit)
                    .HasForeignKey(d => d.IdFamille)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Produit_TP_Producteur");

                entity.HasOne(d => d.IdPlastiqueBaseNavigation)
                    .WithMany(p => p.TpProduitIdPlastiqueBaseNavigation)
                    .HasForeignKey(d => d.IdPlastiqueBase)
                    .HasConstraintName("FK_TP_Produit_TP_Emballage1");

                entity.HasOne(d => d.IdProduitBaseNavigation)
                    .WithMany(p => p.InverseIdProduitBaseNavigation)
                    .HasForeignKey(d => d.IdProduitBase)
                    .HasConstraintName("FK_TP_Produit_TP_Produit");
            });

            modelBuilder.Entity<TpRegulationSolde>(entity =>
            {
                entity.HasKey(e => e.IdRegulationSolde);

                entity.ToTable("TP_Regulation_Solde");

                entity.Property(e => e.IdRegulationSolde).HasColumnName("ID_Regulation_Solde");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdElement).HasColumnName("ID_Element");

                entity.Property(e => e.Motif).HasMaxLength(500);

                entity.Property(e => e.NumDoc)
                    .HasColumnName("NUM_DOC")
                    .HasMaxLength(50);

                entity.Property(e => e.TypeElement).HasColumnName("Type_Element");
            });

            modelBuilder.Entity<TpRegulationStock>(entity =>
            {
                entity.HasKey(e => e.IdRegulationStock);

                entity.ToTable("TP_Regulation_Stock");

                entity.Property(e => e.IdRegulationStock).HasColumnName("ID_Regulation_Stock");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdElement).HasColumnName("ID_Element");

                entity.Property(e => e.Motif).HasMaxLength(500);

                entity.Property(e => e.NumDoc)
                    .HasColumnName("NUM_DOC")
                    .HasMaxLength(50);

                entity.Property(e => e.QuantiteBouteille).HasColumnName("Quantite_Bouteille");

                entity.Property(e => e.QuantiteCasier).HasColumnName("Quantite_Casier");

                entity.Property(e => e.QuantitePlastique).HasColumnName("Quantite_Plastique");

                entity.Property(e => e.TypeElement).HasColumnName("Type_Element");
            });

            modelBuilder.Entity<TpRetourEmballage>(entity =>
            {
                entity.HasKey(e => e.IdRetourEmballage);

                entity.ToTable("TP_Retour_Emballage");

                entity.Property(e => e.IdRetourEmballage).HasColumnName("ID_Retour_Emballage");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdElement).HasColumnName("ID_Element");

                entity.Property(e => e.Motif).HasMaxLength(500);

                entity.Property(e => e.NumDoc)
                    .HasColumnName("NUM_DOC")
                    .HasMaxLength(50);

                entity.Property(e => e.TypeElement).HasColumnName("Type_Element");
            });

            modelBuilder.Entity<TpRetourEmballageDetail>(entity =>
            {
                entity.HasKey(e => e.IdRetourEmballageDetail);

                entity.ToTable("TP_Retour_Emballage_Detail");

                entity.Property(e => e.IdRetourEmballageDetail).HasColumnName("ID_Retour_Emballage_Detail");

                entity.Property(e => e.IdEmballage).HasColumnName("ID_Emballage");

                entity.Property(e => e.IdRetourEmballage).HasColumnName("ID_Retour_Emballage");

                entity.Property(e => e.PuBouteille).HasColumnName("PU_Bouteille");

                entity.Property(e => e.PuPlastique).HasColumnName("PU_Plastique");

                entity.Property(e => e.QuantiteBouteille).HasColumnName("Quantite_Bouteille");

                entity.Property(e => e.QuantiteCasier).HasColumnName("Quantite_Casier");

                entity.Property(e => e.QuantitePlastique).HasColumnName("Quantite_Plastique");

                entity.HasOne(d => d.IdEmballageNavigation)
                    .WithMany(p => p.TpRetourEmballageDetail)
                    .HasForeignKey(d => d.IdEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Retour_Emballage_Detail_TP_Emballage");

                entity.HasOne(d => d.IdRetourEmballageNavigation)
                    .WithMany(p => p.TpRetourEmballageDetail)
                    .HasForeignKey(d => d.IdRetourEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Retour_Emballage_Detail_TP_Retour_Emballage");
            });

            modelBuilder.Entity<TpRetourEmballageTemp>(entity =>
            {
                entity.HasKey(e => e.IdRetourEmballageTemp);

                entity.ToTable("TP_Retour_Emballage_Temp");

                entity.Property(e => e.IdRetourEmballageTemp).HasColumnName("ID_Retour_Emballage_Temp");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.IdEmballage).HasColumnName("ID_Emballage");

                entity.Property(e => e.PuBouteille).HasColumnName("PU_Bouteille");

                entity.Property(e => e.PuPlastique).HasColumnName("PU_Plastique");

                entity.Property(e => e.QuantiteBouteille).HasColumnName("Quantite_Bouteille");

                entity.Property(e => e.QuantiteCasier).HasColumnName("Quantite_Casier");

                entity.Property(e => e.QuantitePlastique).HasColumnName("Quantite_Plastique");

                entity.HasOne(d => d.IdEmballageNavigation)
                    .WithMany(p => p.TpRetourEmballageTemp)
                    .HasForeignKey(d => d.IdEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Retour_Emballage_Fournisseur_Temp_TP_Emballage");
            });

            modelBuilder.Entity<TpRistourneClient>(entity =>
            {
                entity.HasKey(e => e.IdRistourneClient);

                entity.ToTable("TP_Ristourne_Client");

                entity.Property(e => e.IdRistourneClient).HasColumnName("ID_Ristourne_Client");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.CumulerRistourne).HasColumnName("Cumuler_Ristourne");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdClient).HasColumnName("ID_Client");

                entity.Property(e => e.IdProduit).HasColumnName("ID_Produit");

                entity.HasOne(d => d.IdClientNavigation)
                    .WithMany(p => p.TpRistourneClient)
                    .HasForeignKey(d => d.IdClient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Ristourne_Client_TP_Client");

                entity.HasOne(d => d.IdProduitNavigation)
                    .WithMany(p => p.TpRistourneClient)
                    .HasForeignKey(d => d.IdProduit)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Ristourne_Client_TP_Produit");
            });

            modelBuilder.Entity<TpTypeDepense>(entity =>
            {
                entity.HasKey(e => e.IdTypeDepense);

                entity.ToTable("TP_Type_Depense");

                entity.Property(e => e.IdTypeDepense).HasColumnName("ID_Type_Depense");

                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Libelle)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TpVente>(entity =>
            {
                entity.HasKey(e => e.IdVente);

                entity.ToTable("TP_Vente");

                entity.Property(e => e.IdVente).HasColumnName("ID_Vente");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("Create_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.EditCodeUser).HasColumnName("Edit_Code_User");

                entity.Property(e => e.EditDate)
                    .HasColumnName("Edit_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdCaisse).HasColumnName("ID_Caisse");

                entity.Property(e => e.IdClient).HasColumnName("ID_Client");

                entity.Property(e => e.MontantConsigneEmballage).HasColumnName("Montant_Consigne_Emballage");

                entity.Property(e => e.NomClient)
                    .IsRequired()
                    .HasColumnName("Nom_Client")
                    .HasMaxLength(150);

                entity.Property(e => e.NumDoc)
                    .HasColumnName("NUM_DOC")
                    .HasMaxLength(50);

                entity.Property(e => e.SoldeEmballage).HasColumnName("Solde_Emballage");

                entity.Property(e => e.SoldeLiquide).HasColumnName("Solde_Liquide");

                entity.HasOne(d => d.IdCaisseNavigation)
                    .WithMany(p => p.TpVente)
                    .HasForeignKey(d => d.IdCaisse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_TP_Caisse");

                entity.HasOne(d => d.IdClientNavigation)
                    .WithMany(p => p.TpVente)
                    .HasForeignKey(d => d.IdClient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_TP_Client");
            });

            modelBuilder.Entity<TpVenteEmballage>(entity =>
            {
                entity.HasKey(e => e.IdVenteEmballage);

                entity.ToTable("TP_Vente_Emballage");

                entity.Property(e => e.IdVenteEmballage).HasColumnName("ID_Vente_Emballage");

                entity.Property(e => e.IdEmballage).HasColumnName("ID_Emballage");

                entity.Property(e => e.IdVente).HasColumnName("ID_Vente");

                entity.Property(e => e.PuBouteille).HasColumnName("PU_Bouteille");

                entity.Property(e => e.PuPlastique).HasColumnName("PU_Plastique");

                entity.Property(e => e.QuantiteBouteille).HasColumnName("Quantite_Bouteille");

                entity.Property(e => e.QuantiteCasier).HasColumnName("Quantite_Casier");

                entity.Property(e => e.QuantitePlastique).HasColumnName("Quantite_Plastique");

                entity.HasOne(d => d.IdEmballageNavigation)
                    .WithMany(p => p.TpVenteEmballage)
                    .HasForeignKey(d => d.IdEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_Emballage_TP_Emballage");

                entity.HasOne(d => d.IdVenteNavigation)
                    .WithMany(p => p.TpVenteEmballage)
                    .HasForeignKey(d => d.IdVente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_Emballage_TP_Vente");
            });

            modelBuilder.Entity<TpVenteEmballageTemp>(entity =>
            {
                entity.HasKey(e => e.IdVenteEmballageTemp);

                entity.ToTable("TP_Vente_Emballage_Temp");

                entity.Property(e => e.IdVenteEmballageTemp).HasColumnName("ID_Vente_Emballage_Temp");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.IdEmballage).HasColumnName("ID_Emballage");

                entity.Property(e => e.PuBouteille).HasColumnName("PU_Bouteille");

                entity.Property(e => e.PuPlastique).HasColumnName("PU_Plastique");

                entity.Property(e => e.QuantiteBouteille).HasColumnName("Quantite_Bouteille");

                entity.Property(e => e.QuantiteCasier).HasColumnName("Quantite_Casier");

                entity.Property(e => e.QuantitePlastique).HasColumnName("Quantite_Plastique");

                entity.HasOne(d => d.IdEmballageNavigation)
                    .WithMany(p => p.TpVenteEmballageTemp)
                    .HasForeignKey(d => d.IdEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_Emballage_Temp_TP_Emballage");
            });

            modelBuilder.Entity<TpVenteLiquide>(entity =>
            {
                entity.HasKey(e => e.IdVenteLiquide);

                entity.ToTable("TP_Vente_Liquide");

                entity.Property(e => e.IdVenteLiquide).HasColumnName("ID_Vente_Liquide");

                entity.Property(e => e.CumulerRistourne).HasColumnName("Cumuler_Ristourne");

                entity.Property(e => e.IdProduit).HasColumnName("ID_Produit");

                entity.Property(e => e.IdVente).HasColumnName("ID_Vente");

                entity.Property(e => e.ProduitDecomposer).HasColumnName("Produit_Decomposer");

                entity.Property(e => e.PuAchat).HasColumnName("PU_Achat");

                entity.Property(e => e.PuBouteille).HasColumnName("PU_Bouteille");

                entity.Property(e => e.PuPlastique).HasColumnName("PU_Plastique");

                entity.Property(e => e.PuVente).HasColumnName("PU_Vente");

                entity.Property(e => e.QuantiteBouteilleEmballage).HasColumnName("Quantite_Bouteille_Emballage");

                entity.Property(e => e.QuantiteBouteilleLiquide).HasColumnName("Quantite_Bouteille_Liquide");

                entity.Property(e => e.QuantiteCasierEmballage).HasColumnName("Quantite_Casier_Emballage");

                entity.Property(e => e.QuantiteCasierLiquide).HasColumnName("Quantite_Casier_Liquide");

                entity.Property(e => e.QuantitePlastiqueEmballage).HasColumnName("Quantite_Plastique_Emballage");

                entity.Property(e => e.RistourneClient).HasColumnName("Ristourne_Client");

                entity.HasOne(d => d.IdProduitNavigation)
                    .WithMany(p => p.TpVenteLiquide)
                    .HasForeignKey(d => d.IdProduit)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_Liquide_TP_Produit");

                entity.HasOne(d => d.IdVenteNavigation)
                    .WithMany(p => p.TpVenteLiquide)
                    .HasForeignKey(d => d.IdVente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_Liquide_TP_Vente");
            });

            modelBuilder.Entity<TpVenteLiquideTemp>(entity =>
            {
                entity.HasKey(e => e.IdVenteLiquideTemp);

                entity.ToTable("TP_Vente_Liquide_Temp");

                entity.Property(e => e.IdVenteLiquideTemp).HasColumnName("ID_Vente_Liquide_Temp");

                entity.Property(e => e.CreateCodeUser).HasColumnName("Create_Code_User");

                entity.Property(e => e.CumulerRistourne).HasColumnName("Cumuler_Ristourne");

                entity.Property(e => e.IdClient).HasColumnName("ID_Client");

                entity.Property(e => e.IdProduit).HasColumnName("ID_Produit");

                entity.Property(e => e.ProduitDecomposer).HasColumnName("Produit_Decomposer");

                entity.Property(e => e.PuAchat).HasColumnName("PU_Achat");

                entity.Property(e => e.PuBouteille).HasColumnName("PU_Bouteille");

                entity.Property(e => e.PuPlastique).HasColumnName("PU_Plastique");

                entity.Property(e => e.PuVente).HasColumnName("PU_Vente");

                entity.Property(e => e.QuantiteBouteilleEmballage).HasColumnName("Quantite_Bouteille_Emballage");

                entity.Property(e => e.QuantiteBouteilleLiquide).HasColumnName("Quantite_Bouteille_Liquide");

                entity.Property(e => e.QuantiteCasierEmballage).HasColumnName("Quantite_Casier_Emballage");

                entity.Property(e => e.QuantiteCasierLiquide).HasColumnName("Quantite_Casier_Liquide");

                entity.Property(e => e.QuantitePlastiqueEmballage).HasColumnName("Quantite_Plastique_Emballage");

                entity.Property(e => e.RistourneClient).HasColumnName("Ristourne_Client");

                entity.HasOne(d => d.IdClientNavigation)
                    .WithMany(p => p.TpVenteLiquideTemp)
                    .HasForeignKey(d => d.IdClient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_Liquide_Temp_TP_Client");

                entity.HasOne(d => d.IdProduitNavigation)
                    .WithMany(p => p.TpVenteLiquideTemp)
                    .HasForeignKey(d => d.IdProduit)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_Liquide_Temp_TP_Produit");
            });
        }
    }
}
