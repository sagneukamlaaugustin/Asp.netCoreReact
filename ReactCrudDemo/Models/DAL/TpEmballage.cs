﻿using System;
using System.Collections.Generic;

namespace ReactCrudDemo.Models.DAL
{
    public partial class TpEmballage
    {
        public TpEmballage()
        {
            TpCommandeEmballage = new HashSet<TpCommandeEmballage>();
            TpCommandeEmballageTemp = new HashSet<TpCommandeEmballageTemp>();
            TpProduitIdBouteilleBaseNavigation = new HashSet<TpProduit>();
            TpProduitIdEmballageNavigation = new HashSet<TpProduit>();
            TpProduitIdPlastiqueBaseNavigation = new HashSet<TpProduit>();
            TpRetourEmballageDetail = new HashSet<TpRetourEmballageDetail>();
            TpRetourEmballageTemp = new HashSet<TpRetourEmballageTemp>();
            TpVenteEmballage = new HashSet<TpVenteEmballage>();
            TpVenteEmballageTemp = new HashSet<TpVenteEmballageTemp>();
        }

        public int IdEmballage { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int Conditionnement { get; set; }
        public double PuPlastique { get; set; }
        public double PuBouteille { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }
        public int CreateCodeUser { get; set; }
        public int EditCodeUser { get; set; }
        public bool? Actif { get; set; }

        public ICollection<TpCommandeEmballage> TpCommandeEmballage { get; set; }
        public ICollection<TpCommandeEmballageTemp> TpCommandeEmballageTemp { get; set; }
        public ICollection<TpProduit> TpProduitIdBouteilleBaseNavigation { get; set; }
        public ICollection<TpProduit> TpProduitIdEmballageNavigation { get; set; }
        public ICollection<TpProduit> TpProduitIdPlastiqueBaseNavigation { get; set; }
        public ICollection<TpRetourEmballageDetail> TpRetourEmballageDetail { get; set; }
        public ICollection<TpRetourEmballageTemp> TpRetourEmballageTemp { get; set; }
        public ICollection<TpVenteEmballage> TpVenteEmballage { get; set; }
        public ICollection<TpVenteEmballageTemp> TpVenteEmballageTemp { get; set; }
    }
}
