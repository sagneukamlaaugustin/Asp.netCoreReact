﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReactCrudDemo.Models.DAL;
using ReactCrudDemo.Models.VML.TP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ReactCrudDemo.Models.BML.TP
{
    public class ProduitBM
    {
        private TpProduit Local_Var = new TpProduit();
        private BD_FacturationContext db = new BD_FacturationContext();

        public async Task<IEnumerable<ProduitVM>> GetAllData()
        {
            try
            {
                return await (from c in db.TpProduit
                        where c.Actif == true
                        orderby c.Libelle
                        select new ProduitVM
                        {
                            ID_Produit = c.IdProduit,
                            Famille = c.IdFamilleNavigation.Libelle,
                            Emballage = c.IdEmballageNavigation.Code,
                            Libelle = c.Libelle,
                            PU_Achat = c.PuAchat,
                            PU_Vente = c.PuVente,
                            Code = c.Code,
                            Valeur_Emballage = c.ValeurEmballage,
                            Produit_Decomposer = c.ProduitDecomposer,
                            Actif = (bool)c.Actif
                        }).ToListAsync();
            }
            catch
            {
                throw;
            }
        }

        public async Task SaveAsync(ProduitVM Param_Var, string Code_User = "2")
        {
            Local_Var.IdEmballage = Param_Var.ID_Emballage;
            Local_Var.IdFamille = Param_Var.ID_Famille;
            Local_Var.Code = Param_Var.Code;
            Local_Var.Libelle = Param_Var.Libelle;
            Local_Var.PuAchat = Param_Var.PU_Achat;
            Local_Var.PuVente = Param_Var.PU_Vente;
            Local_Var.ValeurEmballage = Param_Var.Valeur_Emballage;
            Local_Var.ProduitDecomposer = Param_Var.Produit_Decomposer;
            if(Param_Var.Produit_Decomposer)
            {
                Local_Var.IdProduitBase = Param_Var.ID_Produit_Base;
                Local_Var.IdPlastiqueBase = Param_Var.ID_Plastique_Base;
                Local_Var.IdBouteilleBase = Param_Var.ID_Bouteille_Base;
            }
            else
            {
                Local_Var.IdProduitBase = null;
                Local_Var.IdPlastiqueBase = null;
                Local_Var.IdBouteilleBase = null;
            }
            Local_Var.EditCodeUser = int.Parse(Code_User);
            Local_Var.CreateCodeUser = int.Parse(Code_User);
            Local_Var.CreateDate = DateTime.Now;
            Local_Var.EditDate = DateTime.Now;
            Local_Var.Actif = true;
            db.TpProduit.Add(Local_Var);
            try
            {
                await db.SaveChangesAsync();
            }
            catch
            {
                throw;
            }
           
        }

        public async Task EditAsync(TpProduit tpProduit, ProduitVM Param_Var, string Code_User = "2")
        {
            Local_Var = tpProduit;
            Local_Var.IdEmballage = Param_Var.ID_Emballage;
            Local_Var.IdFamille = Param_Var.ID_Famille;
            Local_Var.Libelle = Param_Var.Libelle;
            Local_Var.Code = Param_Var.Code;
            Local_Var.PuAchat = Param_Var.PU_Achat;
            Local_Var.PuVente = Param_Var.PU_Vente;
            Local_Var.ValeurEmballage = Param_Var.Valeur_Emballage;
            Local_Var.ProduitDecomposer = Param_Var.Produit_Decomposer;
            if (Param_Var.Produit_Decomposer)
            {
                Local_Var.IdProduitBase = Param_Var.ID_Produit_Base;
                Local_Var.IdPlastiqueBase = Param_Var.ID_Plastique_Base;
                Local_Var.IdBouteilleBase = Param_Var.ID_Bouteille_Base;
            }
            else
            {
                Local_Var.IdProduitBase = null;
                Local_Var.IdPlastiqueBase = null;
                Local_Var.IdBouteilleBase = null;
            }
            Local_Var.EditCodeUser = int.Parse(Code_User);
            Local_Var.EditDate = DateTime.Now;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw; 
            }
        }

        public async Task DelAsync(TpProduit item)
        {
            db.TpProduit.Remove(item);
            await db.SaveChangesAsync();
        }

        private bool IsExists(int id)
        {
            return db.TpProduit.Any(e => e.IdProduit == id);
        }


        public async Task<TpProduit> GetItemAsync(int id)
        {
            return await db.TpProduit.SingleOrDefaultAsync(m => m.IdProduit == id);
        }

            
        public ProduitVM GetByID(int id)
        {
            TpProduit LocalObjet = db.TpProduit.Include(p => p.IdEmballageNavigation)
                                               .Include(p => p.IdFamilleNavigation)
                                               .FirstOrDefault(p => p.IdProduit == id);
            ProduitVM MidelObjet = new ProduitVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.ID_Produit = id;
            MidelObjet.ID_Emballage = LocalObjet.IdEmballage;
            MidelObjet.ID_Famille = LocalObjet.IdFamille;
            MidelObjet.Famille = LocalObjet.IdFamilleNavigation.Libelle;
            MidelObjet.Libelle = LocalObjet.Libelle;
            MidelObjet.Code = LocalObjet.Code;
            MidelObjet.PU_Achat = LocalObjet.PuAchat;
            MidelObjet.PU_Vente = LocalObjet.PuVente;
            MidelObjet.Valeur_Emballage = LocalObjet.ValeurEmballage;
            MidelObjet.Produit_Decomposer = LocalObjet.ProduitDecomposer;
            MidelObjet.ID_Produit_Base = LocalObjet.IdProduitBase;
            MidelObjet.ID_Plastique_Base = LocalObjet.IdPlastiqueBase;
            MidelObjet.ID_Bouteille_Base = LocalObjet.IdBouteilleBase;
            MidelObjet.Actif = (bool)LocalObjet.Actif;         
            return MidelObjet;
        }

        /*
               public bool IsNotExist(ProduitVM Param_Var, ref string MessagesRetour)
               {
                   // si l'élément n'existe pas
                   if (db.TP_Produit.Where(p => p.Libelle == Param_Var.Libelle).FirstOrDefault() == null)
                   {
                       return true;
                   }
                   else if (db.TP_Produit.Where(p => p.Libelle == Param_Var.Libelle).Where(p => p.Actif == true).FirstOrDefault() != null)
                   {
                       MessagesRetour = string.Format("Le produit {0} existe déjà", Param_Var.Libelle);
                       // l'élément existe et es consultable
                       return false;
                   }
                   else
                   {
                       // si l'élément existe et est dans la corbeille
                       MessagesRetour = string.Format("Le produit {0} existe déjà mais est actuellement supprimé, veuillez contacter un adminstrateur pour le restorer");
                       return false;
                   }
               }

               /// <summary>
               /// verifie s'il existe un element avec le meme libelle 
               /// et l'identifiant different
               /// </summary>
               /// <param name="libelle"></param>
               /// <param name="id"></param>
               /// <returns></returns>
               public bool IsNotExist(ProduitVM Param_Var, int id, ref string MessagesRetour)
               {
                   // si l'élément n'existe pas
                   if (db.TP_Produit.Where(p => p.Libelle == Param_Var.Libelle).Where(p => p.ID_Produit != id).FirstOrDefault() == null)
                   {
                       return true;
                   }
                   else if (db.TP_Produit.Where(p => p.Libelle == Param_Var.Libelle).Where(p => p.ID_Produit != id).Where(p => p.Actif == true).FirstOrDefault() != null)
                   {
                       MessagesRetour = string.Format("Le produit {0} existe déjà", Param_Var.Libelle);
                       // l'élément existe et es consultable
                       return false;
                   }
                   else
                   {
                       // si l'élément existe et est dans la corbeille
                       MessagesRetour = string.Format("Le produit  {0} existe déjà mais est actuellement supprimé, veuillez contacter un adminstrateur pour le restorer");
                       return false;
                   }
               }

               public Boolean VerifieEtatStock(int ID_Produit, int Casier,int Bouteille, ref string MessageRetour)
               {
                   var produit = db.TP_Produit.Find(ID_Produit);
                   if(produit.Produit_Decomposer == true)
                   {
                       produit = db.TP_Produit.Find(produit.ID_Produit_Base);
                   }
                   int quantite = Casier * produit.TP_Emballage.Conditionnement + Bouteille;
                   var stock = db.FN_Etat_Stock(0, DateTime.Now, DateTime.Now).Where(p => p.ID_Element == ID_Produit).FirstOrDefault();
                   if(quantite <= (stock.Stock_Casier * stock.Conditionnement + stock.Stock_Bouteille))
                   {
                       MessageRetour = string.Format("Quantité en stock insuffisante. Casier : {0}, Bouteille {1} ", stock.Stock_Casier, stock.Stock_Bouteille);
                       return false;
                   }
                   return true;
               }
               */
    }
}