﻿using System.ComponentModel.DataAnnotations;

namespace ReactCrudDemo.Models.VML.TP
{
    // [Bind(Exclude = "ID_Famille")]
    public class FamilleVM
    {
        public int ID_Famille { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Libelle * :")]
        [StringLength(50, ErrorMessage = "Le Libelle doit contenir au plus 50 caractères")]
        public string Libelle { get; set; }

        public bool Actif { get; set; }

        public string Statut => Actif ? "Actif" : "Suspendu";
    }
}