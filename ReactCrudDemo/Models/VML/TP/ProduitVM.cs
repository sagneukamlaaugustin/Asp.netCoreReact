﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReactCrudDemo.Models.VML.TP
{
   // [Bind(include: "ID_Produit")]
    public class ProduitVM
    {
        public int ID_Produit { get; set; }

        [Required, Display(Name = "Code * :")]
        [StringLength(50, ErrorMessage = "Le code doit contenir au plus 50 caractères")]
        public string Code { get; set; }

        [Required, Display(Name = "Conditionnement * :")]
        public int ID_Emballage { get; set; }
        public string Emballage { get; set; }

        [Required, Display(Name = "Famille * :")]
        public int ID_Famille { get; set; }
        public string Famille { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Désignation * :")]
        [StringLength(150, ErrorMessage = "Ce champs  doit contenir au plus 150 caractères")]
        public string Libelle { get; set; }

        [Display(Name = "Valeur Emballage? * :")]
        public bool Valeur_Emballage { get; set; }
        public string Valeur_Emballage_ => (Valeur_Emballage) ? "Oui" : "Non";

        [Display(Name = "Produit Décomposé? * :")]
        public bool Produit_Decomposer { get; set; }
        public string Produit_Decomposer_ => (Produit_Decomposer) ? "Oui" : "Non";

        [Display(Name = "PU Vente * :")]
        [Range(1, double.MaxValue, ErrorMessage = "Le PU Vente doit être supérieur au 0!")]
        public double PU_Vente { get; set; }

        [Display(Name = "PU Achat * :")]
        [Range(1, double.MaxValue, ErrorMessage = "Le PU Achat doit être supérieur au 0!")]
        public double PU_Achat { get; set; }

        [Display(Name = "Produit De Base * :")]
        public int? ID_Produit_Base { get; set; }
        [Display(Name = "Plastique * :")]
        public int? ID_Plastique_Base { get; set; }
        [Display(Name = "Bouteille * :")]
        public int? ID_Bouteille_Base { get; set; }

        public bool Actif { get; set; }
        public string Statut => (Actif) ? "Actif" : "Suspendu";   
    }
}