
//import './jqwidgets/jqxcore.js';
//import './jqwidgets/jqxgrid.js';
//import './jqwidgets/jqxgrid.selection.js';
//import './jqwidgets/jqxgrid.pager.js';
//import './jqwidgets/jqxlistbox.js';
//import './jqwidgets/jqxbuttons.js';
//import './jqwidgets/jqxscrollbar.js';
//import './jqwidgets/jqxdatatable.js';
//import './jqwidgets/jqxtreegrid.js';
//import './jqwidgets/jqxmenu.js';
//import './jqwidgets/jqxcalendar.js';
//import './jqwidgets/jqxgrid.sort.js';
//import './jqwidgets/jqxgrid.filter.js';
//import './jqwidgets/jqxdatetimeinput.js';
//import './jqwidgets/jqxdropdownlist.js';
//import './jqwidgets/jqxslider.js';
//import './jqwidgets/jqxeditor.js';
//import './jqwidgets/jqxinput.js';
//import './jqwidgets/jqxdraw.js';
//import './jqwidgets/jqxchart.core.js';
//import './jqwidgets/jqxchart.rangeselector.js';
//import './jqwidgets/jqxtree.js';
//import './jqwidgets/globalization/globalize.js';
//import './jqwidgets/jqxbulletchart.js';
//import './jqwidgets/jqxcheckbox.js';
//import './jqwidgets/jqxradiobutton.js';
//import './jqwidgets/jqxvalidator.js';
//import './jqwidgets/jqxpanel.js';
//import './jqwidgets/jqxpasswordinput.js';
//import './jqwidgets/jqxnumberinput.js';
//import './jqwidgets/jqxcombobox.js';

//import './jqwidgets/styles/jqx.base.css';
//import './jqwidgets/styles/jqx.arctic.css';
//import './jqwidgets/styles/jqx.black.css';
//import './jqwidgets/styles/jqx.bootstrap.css';
//import './jqwidgets/styles/jqx.classic.css';
//import './jqwidgets/styles/jqx.darkblue.css';
//import './jqwidgets/styles/jqx.energyblue.css';
//import './jqwidgets/styles/jqx.fresh.css';
//import './jqwidgets/styles/jqx.highcontrast.css';
//import './jqwidgets/styles/jqx.metro.css';
//import './jqwidgets/styles/jqx.metrodark.css';
//import './jqwidgets/styles/jqx.office.css';
//import './jqwidgets/styles/jqx.orange.css';
//import './jqwidgets/styles/jqx.shinyblack.css';
//import './jqwidgets/styles/jqx.summer.css';
//import './jqwidgets/styles/jqx.web.css';
//import './jqwidgets/styles/jqx.ui-darkness.css';
////import './jqwidgets/styles/jqx.ui-lightness.css';
////import './jqwidgets/styles/jqx.ui-le-frog.css';
////import './jqwidgets/styles/jqx.ui-overcast.css';
////import './jqwidgets/styles/jqx.ui-redmond.css';
//import './jqwidgets/styles/jqx.ui-smoothness.css';
////import './jqwidgets/styles/jqx.ui-start.css';
////import './jqwidgets/styles/jqx.ui-sunny.css';
////import './jqwidgets/styles/bootstrap.css';

import './css/site.css';
import 'bootstrap';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { BrowserRouter } from 'react-router-dom';
import * as RoutesModule from './routes';
let routes = RoutesModule.routes;

function renderApp() {
    // This code starts up the React app when it runs in a browser. It sets up the routing
    // configuration and injects the app into a DOM element.
    const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href')!;
    ReactDOM.render(
        <AppContainer>
            <BrowserRouter children={ routes } basename={ baseUrl } />
        </AppContainer>,
        document.getElementById('react-app')
    );
}

renderApp();

// Allow Hot Module Replacement
if (module.hot) {
    module.hot.accept('./routes', () => {
        routes = require<typeof RoutesModule>('./routes').routes;
        renderApp();
    });
}
