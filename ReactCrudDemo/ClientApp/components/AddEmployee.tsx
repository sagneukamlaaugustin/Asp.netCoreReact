﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { ProduitData } from './FetchEmployee';

interface AddEmployeeDataState {
    title: string;
    loading: boolean;
    familleList: Array<any>;
    emballageList: Array<any>;
    empData: ProduitData;
}

export class AddEmployee extends React.Component<RouteComponentProps<{}>, AddEmployeeDataState> {
    constructor(props) {
        super(props);

        this.state = { title: "", loading: true, familleList: [], emballageList: [], empData: new ProduitData };

        fetch('api/Famille/Index')
            .then(response => response.json() as Promise<Array<any>>)
            .then(data => {
                this.setState({ familleList: data });
            });

        fetch('api/Emballage/Index')
            .then(response => response.json() as Promise<Array<any>>)
            .then(data => {
                this.setState({ emballageList: data });
            });

        var empid = this.props.match.params["empid"];

        // This will set state for Edit employee
        if (empid > 0) {
            fetch('api/Produits/Details/' + empid)
                .then(response => response.json() as Promise<ProduitData>)
                .then(data => {
                    this.setState({ title: "Edit", loading: false, empData: data });
                });
        }

        // This will set state for Add Produits
        else {
            this.state = { title: "Create", loading: false, familleList: [], emballageList: [], empData: new ProduitData };
        }

        // This binding is necessary to make "this" work in the callback
        this.handleSave = this.handleSave.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderCreateForm(this.state.familleList, this.state.emballageList);

        return <div>
            <h1>{this.state.title}</h1>
            <h3>Produit</h3>
            <hr />
            {contents}
        </div>;
    }

    // This will handle the submit form event.
    private handleSave(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        // PUT request for Edit Produits.
        if (this.state.empData.iD_Produit) {
            fetch('api/Produits/Edit', {
                method: 'PUT',
                body: data,

            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/fetchemployee");
                })
        }

        // POST request for Add employee.
        else {
            fetch('api/Produits/Create', {
                method: 'POST',
                body: data,

            }).then((response) => response.json())
                .then((responseJson) => {
                    this.props.history.push("/fetchemployee");
                })
        }
    }

    // This will handle Cancel button click event.
    private handleCancel(e) {
        e.preventDefault();
        this.props.history.push("/fetchemployee");
    }

    // Returns the HTML Form to the render() method.
    private renderCreateForm(familleList: Array<any>, emballageList: Array<any>) {
        return (
            <form onSubmit={this.handleSave} >
                <div className="form-group row" >
                    <input type="hidden" name="iD_Produit" value={this.state.empData.iD_Produit} />
                </div>
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="code">Code</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="code" defaultValue={this.state.empData.code} required />
                    </div>
                </div >
                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="libelle">Libelle</label>
                    <div className="col-md-4">
                        <input className="form-control" type="text" name="libelle" defaultValue={this.state.empData.libelle} required />
                    </div>
                </div >

                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="produit_Decomposer">Produit Decomposer ?</label>
                    <div className="col-md-4">
                        <input className="form-control" type="checkbox" name="produit_Decomposer" />
                    </div>
                </div >

                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="valeur_Emballage">Valeur Emballage ?</label>
                    <div className="col-md-4">
                        <input className="form-control" type="checkbox" name="valeur_Emballage" />
                    </div>
                </div >

                < div className="form-group row" >
                    <label className=" control-label col-md-12" htmlFor="pU_Vente">PU Vente</label>
                    <div className="col-md-4">
                        <input className="form-control" type="number" name="pU_Vente" defaultValue={this.state.empData.pU_Vente.toString()} required/>
                    </div>
                </div >        
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="pU_Achat" >PU Achat</label>
                    <div className="col-md-4">
                        <input className="form-control" type="number" name="pU_Achat" defaultValue={this.state.empData.pU_Achat.toString()} required/>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="iD_Emballage">Emballage</label>
                    <div className="col-md-4">
                        <select className="form-control" data-val="true" name="iD_Emballage" defaultValue={this.state.empData.iD_Emballage.toString()} required>
                            <option value="">-- Select Emballage --</option>
                            {emballageList.map(city =>
                                <option key={city.iD_Emballage} value={city.iD_Emballage}>{city.code}</option>
                            )}
                        </select>
                    </div>
                </div >
                <div className="form-group row">
                    <label className="control-label col-md-12" htmlFor="iD_Famille">Famille</label>
                    <div className="col-md-4">
                        <select className="form-control" data-val="true" name="iD_Famille" defaultValue={this.state.empData.iD_Famille.toString()} required>
                            <option value="">-- Select Famille --</option>
                            {familleList.map(city =>
                                <option key={city.iD_Famille} value={city.iD_Famille}>{city.libelle}</option>
                            )}
                        </select>
                    </div>
                </div >
                <div className="form-group">
                    <button type="submit" className="btn btn-default">Save</button>
                    <button className="btn" onClick={this.handleCancel}>Cancel</button>
                </div >
            </form >
        )
    }
}