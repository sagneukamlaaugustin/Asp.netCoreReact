﻿//import JqxGrid from '../jqwidgets-react/react_jqxgrid.js';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';

interface FetchProduitDataState {
    produitList: ProduitData[];
    loading: boolean;
}

export class FetchEmployee extends React.Component<RouteComponentProps<{}>, FetchProduitDataState> {
    constructor() {
        super();
        this.state = { produitList: [], loading: true };

        fetch('api/Produits/Index')
            .then(response => response.json() as Promise<ProduitData[]>)
            .then(data => {
                this.setState({ produitList: data, loading: false });
            });

       // This binding is necessary to make "this" work in the callback
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);

    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderEmployeeTable(this.state.produitList);

        return <div>
            <h1>Employee Data</h1>
            <p>This component demonstrates fetching Employee data from the server.</p>
            <p>
                <Link to="/addemployee">Create New</Link>
            </p>
            {contents}
        </div>;
    }

    // Handle Delete request for an employee
    private handleDelete(id: number, Libelle: string) {
        if (!confirm("Do you want to delete employee with Id: " + Libelle))
            return;
        else {
            fetch('api/Produits/Delete/' + id, {
                method: 'delete'
            }).then(data => {
                this.setState(
                    {
                        produitList: this.state.produitList.filter((rec) => {
                            return (rec.iD_Produit != id);
                        })
                    });
            });
        }
    }

    private handleEdit(id: number) {
        this.props.history.push("/Produits/edit/" + id);
    }

    //// Returns the HTML table to the render() method.
    //private renderEmployeeTable(produitList: ProduitData[]) {

    //  // let dataAdapter = new JqxGrid.dataAdapter(produitList);

    //    let columns =
    //        [
    //            { text: 'Code', columngroup: 'ProductDetails', datafield: 'code', width: 100 },
    //            { text: 'Libelle', columngroup: 'ProductDetails', datafield: 'libelle', width: 150 },
    //            { text: 'Emballage', columngroup: 'ProductDetails', datafield: 'emballage', width: 100 },
    //            { text: 'Famille', columngroup: 'ProductDetails', datafield: 'famille', width: 100 },
    //            { text: 'Valeur Emballage', columngroup: 'ProductDetails', datafield: 'valeur_Emballage_', width: 100 },
    //            { text: 'Produit Decomposé', columngroup: 'ProductDetails', datafield: 'produit_Decomposer_', width: 100 },
    //            { text: 'Prix Vente', columngroup: 'ProductDetails', datafield: 'pU_Vente', align: 'right', cellsalign: 'right', cellsformat: 'c2', width: 200 },
    //            { text: 'Prix Achat', columngroup: 'ProductDetails', datafield: 'pU_Achat', align: 'right', cellsalign: 'right', cellsformat: 'c2', width: 200 },
    //            { text: 'Statut', columngroup: 'ProductDetails', datafield: 'statut', width: 100 },
    //            { text: 'Discontinued', columntype: 'checkbox', datafield: 'Discontinued' }
    //        ];

    //    let columngroups =
    //        [
    //            { text: 'Product Details', align: 'center', name: 'ProductDetails' }
    //        ];
    //    return ( 
    //        <JqxGrid
    //            width={850} source={produitList} pageable={true}
    //            sortable={true} altrows={true} enabletooltips={true}
    //            autoheight={true} editable={true} columns={columns}
    //            selectionmode={'multiplecellsadvanced'} columngroups={columngroups}
    //        />
    //    );
    //}

     // Returns the HTML table to the render() method.
    private renderEmployeeTable(produitList: ProduitData[]) {
        return  <table className='table'>
            <thead>
                <tr>
                    <th></th>
                    <th>Code</th>
                    <th>Libelle</th>
                    <th>Emballage</th>
                    <th>Famille</th>
                    <th>Valeur Emaballage</th>
                    <th>Produit Decompose ?</th>
                    <th>Prix Vente</th>
                    <th>Prix Achat</th>
                    <th>Statut</th>
                </tr>
            </thead>
            <tbody>
                {produitList.map(pro =>
                    <tr key={pro.iD_Produit}>
                        <td></td>
                        <td>{pro.code}</td>
                        <td>{pro.libelle}</td>
                        <td>{pro.emballage}</td>
                        <td>{pro.famille}</td>
                        <td>{pro.valeur_Emballage_}</td>
                        <td>{pro.produit_Decomposer_}</td>
                        <td>{pro.pU_Vente}</td>
                        <td>{pro.pU_Achat}</td>
                        <td>{pro.statut}</td>
                        <td>
                            <a className="action" onClick={(id) => this.handleEdit(pro.iD_Produit)}>Edit</a>  |
                            <a className="action" onClick={(id) => this.handleDelete(pro.iD_Produit, pro.libelle)}>Delete</a>
                        </td>
                    </tr>
                )}
            </tbody>
        </table>;
    }
}

export class ProduitData {
    iD_Produit: number = 0;
    code: string = "";
    libelle: string = "";
    emballage: string = "";
    famille: string = ""; 
    iD_Famille: number = 0;
    iD_Emballage: number = 0;
    valeur_Emballage_: string = "";
    produit_Decomposer_: string = "";
    valeur_Emballage: boolean = false;
    produit_Decomposer: boolean = false;
    pU_Vente: number = 0;
    pU_Achat: number = 0;
    statut: string = "";
} 